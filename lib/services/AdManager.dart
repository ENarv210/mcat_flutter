import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';

class AdManager {

  static String get appId {
    if (Platform.isAndroid) {
      //return "ca-app-pub-3940256099942544~4354546703"; //test
      return "ca-app-pub-5846965047567829~7081977236";
    } else if (Platform.isIOS) {
      return "<YOUR_IOS_ADMOB_APP_ID>";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>[
      'MCAT',
      'Tutoring',
      'STEM',
      'Science',
      'Registered Nurse',
      'RN',
      'Medical',
      'Medicine',
      'Exam',
      'Medical School',
      'Caribbean School',
      'NCLEX',
      'STEP 1',
      'STEP 2',
      'USMLE'
    ],
    contentUrl: 'https://www.mcat.study',
    childDirected: false,
    testDevices: <String>[],
  );



  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      //return "ca-app-pub-3940256099942544/8865242552"; //test
      return "ca-app-pub-5846965047567829/4455813894";
    } else if (Platform.isIOS) {
      return "<YOUR_IOS_BANNER_AD_UNIT_ID>";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get interstitialAdUnitId {
    if (Platform.isAndroid) {
      return "<YOUR_ANDROID_INTERSTITIAL_AD_UNIT_ID>";
    } else if (Platform.isIOS) {
      return "<YOUR_IOS_INTERSTITIAL_AD_UNIT_ID>";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get rewardedAdUnitId {
    if (Platform.isAndroid) {
      return "<YOUR_ANDROID_REWARDED_AD_UNIT_ID>";
    } else if (Platform.isIOS) {
      return "<YOUR_IOS_REWARDED_AD_UNIT_ID>";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
}