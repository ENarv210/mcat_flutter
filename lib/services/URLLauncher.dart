import 'package:url_launcher/url_launcher.dart';

class URLLauncher {
  URLLauncher();

  Future<void> launchURLWithJavaScript(String url) async {
    print("Link:" + url);

    if (await canLaunch(url)) {
      print("Launching");
      await launch(
        url,
        forceSafariVC: true,
        forceWebView: false,
        enableJavaScript: true,
      );
    } else {
      print("Could Not Launch");
      throw 'Could not launch $url';
    }
  }

  Future<void> closeExistingWebView() async{
    closeWebView();
  }
}