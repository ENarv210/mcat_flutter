class KnowledgeCard {
  final String title;
  final String mcatSection;
  final String data;

  KnowledgeCard({this.title, this.mcatSection, this.data});

  ///   * Conversion of JSON data to variable data
  factory KnowledgeCard.fromJson(Map<String,dynamic> parsedJSON){
    return KnowledgeCard(
      title: parsedJSON['title'] as String,
      mcatSection: parsedJSON['mcat_section'] as String,
      data: parsedJSON['data'] as String,
    );
  }

}