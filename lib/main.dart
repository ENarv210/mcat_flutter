import 'package:flutter/material.dart';
import 'package:mcatreview/pages/HomePage.dart';
import 'package:mcatreview/constants/AppColors.dart' as AppColors;
import 'package:mcatreview/constants/Strings.dart' as StringConstants;

/// Main method of getting into Flutter application execution.
void main() {
  runApp(MyApp());
}

/// Root of Application to extend Stateless Object
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: StringConstants.APP_NAME,
      theme: ThemeData(
        primarySwatch: AppColors.darkBlueTheme,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: StringConstants.APP_NAME),
    );
  }
}

/// Actually extend an Stateful Widget UI interface.
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  HomePage createState() => HomePage();
}


