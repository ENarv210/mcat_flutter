import 'package:flutter/material.dart';

class MainMenuCard {
  String sectionTitle;
  String sectionDetails;
  Icon icon;
}