import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mcatreview/services/GoogleAnalytics.dart';
import 'package:mcatreview/constants/Strings.dart' as StringConstants;
import 'package:mcatreview/services/URLLauncher.dart' as WebLauncher;

class Podcasts extends StatelessWidget{

  Card createJSONCards(String provider, String topic, String link){
    Color cardBackgroundHexColor = Color(0xFF7D26CD);
    return new Card(
      child: Stack(
        children: <Widget>[
          Align(
            // alignment: Alignment.topLeft,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.maxFinite,
                    color: cardBackgroundHexColor,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15, top: 4, bottom: 4),
                      child: Column(
                        children: <Widget>[
                          RichText(
                            textAlign: TextAlign.left,
                            text: TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                  text: provider,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                    fontSize: 25,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: double.maxFinite,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15, top: 50),
                      child: Column(
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                  text: topic,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey,
                                    fontSize: 23,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: double.maxFinite,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 20),
                              child: Column(
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.PODCAST + " : " + link);
                                      WebLauncher.URLLauncher().launchURLWithJavaScript(link);
                                    },
                                    child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: "Tap Here to View",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color: Colors.lightBlue,
                                            fontSize: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    WebLauncher.URLLauncher().closeExistingWebView();

    return Scaffold(
      appBar: AppBar(
        title: Text("MCAT Podcasts"),
      ),

      body:
            StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance.collection('podcasts').snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              Column cardColumn = Column(
                children: [],
              );
              //Create SingleChildScrollView Return Object//
              SingleChildScrollView singleChildScrollView =  SingleChildScrollView(
                child: cardColumn,
              );

              //Create Padding For In Between Cards//
              Padding padding = Padding(
                padding: EdgeInsets.only(bottom: 5),);

              Container adBufferSpace = Container(
                height: 100,
              );

              for (int i=0; i<snapshot.data.documents.length; i++) {
                cardColumn.children.add(createJSONCards(
                    snapshot.data.documents[i]['Provider'].toString(),
                    snapshot.data.documents[i]['Topic'].toString(),
                    snapshot.data.documents[i]['Link'].toString()));
                cardColumn.children.add(padding);
              }

              //Add Ad Buffer Space to Bottom//
              cardColumn.children.add(adBufferSpace);

              return singleChildScrollView;
            } else {
            return LinearProgressIndicator();
            }
          }),
    );
  }
}