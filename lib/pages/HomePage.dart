import 'dart:math';
import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mcatreview/main.dart';
import 'package:mcatreview/constants/AppColors.dart' as AppColors;
import 'package:mcatreview/constants/Strings.dart' as StringConstants;
import 'package:mcatreview/constants/Sizes.dart' as SizeConstants;
import 'package:mcatreview/pages/DataPageTemplate.dart';
import 'package:mcatreview/pages/Podcasts.dart';
import 'package:mcatreview/pages/QuestionsOfDay.dart';
import 'package:mcatreview/services/AdManager.dart';
import 'package:mcatreview/services/GoogleAnalytics.dart';
import 'package:mcatreview/pages/Resources.dart';

class HomePage extends State<MyHomePage> {
  void setupGlobals(){
    //Register Singletons//
    GetIt.instance.registerLazySingleton(() => GoogleAnalytics());
  }

  Future<void> _initAdMob() {
    // TODO: Initialize AdMob SDK
    print("Ad id: " + AdManager.appId);
    return FirebaseAdMob.instance.initialize(appId: AdManager.appId);
  }

  @override
  // ignore: must_call_super
  void initState() {
    //Get Global Values//
    setupGlobals();

    //Set Ad Values//
    _initAdMob();

    BannerAd _bannerAd = BannerAd(
        adUnitId: AdManager.bannerAdUnitId,
        size: AdSize.largeBanner,
      targetingInfo: AdManager.targetingInfo,
      listener: (MobileAdEvent event) {
      print("BannerAd event is $event");
    },
    );

    void _loadBannerAd() {
      _bannerAd
        ..load()
        ..show(anchorType: AnchorType.bottom);
    }

    _loadBannerAd();
  }

  @override
  Widget build(BuildContext context) {
    Random randomNumberWithinRange = new Random();
    return Scaffold(
      appBar: AppBar(
        title: Text(StringConstants.APP_NAME),
      ),
      body:
      SingleChildScrollView(
        child: Column(
          children: <Widget>[
              StreamBuilder<QuerySnapshot>(
              stream: Firestore.instance.collection('imageOfDay').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Image.network(snapshot.data.documents[randomNumberWithinRange.nextInt(snapshot.data.documents.length)]['url'].toString());
                  } else {
                    return LinearProgressIndicator();
                  }
                }),
            //Question of the Day//
            Container(
              padding: EdgeInsets.fromLTRB(3,3,3,3),
              height: 60,
              width: double.maxFinite,
              child: Card(
                color: Colors.lightBlue,
                elevation: 3,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.QUESTIONS_OF_THE_DAY);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => QuestionsOfDay()),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(3),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.center,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(StringConstants.QUESTIONS_OF_THE_DAY,
                                            style: TextStyle(color: Colors.white, fontSize: 23))),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Podcast Section//
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                color: AppColors.darkGreen,
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.PODCAST);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Podcasts()),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.PODCAST,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.white,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.PODCAST_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Resources Section//
            Divider(
                color: Colors.black
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                color: AppColors.darkGreen,
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.RESOURCES);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Resources()),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.RESOURCES,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.white,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.RESOURCES_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            //Selection a Session to Review//
            Container(
              padding: const EdgeInsets.only(bottom: 5, top: 5),
              color: AppColors.darkBlue,
              width: double.infinity,
                    child: Text(StringConstants.SELECT_A_SECTION,
                        style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center),
                ),
            ///////////////////////////////////////Anatomy Section//
            Container(
              color: AppColors.blueDarkAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.ANATOMY);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.ANATOMY,colorScheme: AppColors.blueDarkAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.ANATOMY,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.ANATOMY_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////BioChem Section//
            Container(
              color: AppColors.grayAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.BIOCHEM);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.BIOCHEM,colorScheme: AppColors.grayAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.BIOCHEM,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.BIOCHEM_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Biology Section//
            Container(
              color: AppColors.magentaPink,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.BIOLOGY);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.BIOLOGY,colorScheme: AppColors.magentaPink)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.BIOLOGY,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.BIOLOGY_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////CARS Section//
            Container(
              color: AppColors.purpleAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.CARS);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.CARS,colorScheme: AppColors.purpleAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.CARS,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.CARS_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Chemistry Section//
            Container(
              color: AppColors.greenDarkAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.CHEMISTRY);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.CHEMISTRY,colorScheme: AppColors.greenDarkAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.CHEMISTRY,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.CHEMISTRY_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Genetics Section//
            Container(
              color: AppColors.blueDarkAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.GENETICS);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.GENETICS,colorScheme: AppColors.blueDarkAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.GENETICS,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.GENETICS_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Organic Chemistry Section//
            Container(
              color: AppColors.turquoiseAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.ORGANIC_CHEM);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.ORGANIC_CHEM,colorScheme: AppColors.turquoiseAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.ORGANIC_CHEM,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.ORGANIC_CHEM_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Physics Section//
            Container(
              color: AppColors.greenDarkAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.PHYSICS);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.PHYSICS,colorScheme: AppColors.greenDarkAccent)),
                    );
                  },
                child: Padding(
                  padding: EdgeInsets.all(7),
                  child: Stack(
                    children: <Widget>[
                      Align(
                          alignment: Alignment.centerRight,
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 10, top: 5),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: StringConstants.PHYSICS,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w300,
                                                  color: Colors.black,
                                                  fontSize: 40,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 10, top: 60),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: StringConstants.PHYSICS_DEFINITION,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w300,
                                                  color: Colors.grey,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ))
                    ],
                  ),
                ),
              ),
              ),
            ),
            ///////////////////////////////////////Physiology Section//
            Container(
              color: AppColors.purpleAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.PHYSIOLOGY);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.PHYSIOLOGY,colorScheme: AppColors.purpleAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.PHYSIOLOGY,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.PHYSIOLOGY_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Psychology Section//
            Container(
              color: AppColors.blueLightAccent,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.PSYCHOLOGY);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.PSYCHOLOGY,colorScheme: AppColors.blueLightAccent)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.PSYCHOLOGY,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.PSYCHOLOGY_DEFINITION,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Sociology Section//
            Container(
              color: AppColors.magentaPink,
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              height: SizeConstants.HOME_PAGE_CARD_SIZE,
              width: double.maxFinite,
              child: Card(
                elevation: SizeConstants.HOME_PAGE_CARD_ELEVATION,
                child: new InkWell(
                  onTap: () {
                    GoogleAnalytics().googleAnalytics.logEvent(name: StringConstants.SOCIOLOGY);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DataPageTemplate(jsonFileName: StringConstants.SOCIOLOGY,colorScheme: AppColors.magentaPink)),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(7),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.SOCIOLOGY,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.black,
                                                    fontSize: 40,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 60),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: StringConstants.SOCIOLOGY,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////Version Number//
            Container(
              padding: EdgeInsets.fromLTRB(
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS,
                  SizeConstants.HOME_PAGE_CARD_INSETS),
              child: Text("Version 1.0.0"),
              ),
            ///////////////////////////////////////Ad Space Buffer//
            Container(
              height: 100,
            )
        ],
        ),
      ),
    );
  }
}