import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mcatreview/models/KnowledgeCard.dart';
import 'package:flutter/services.dart' as DartService show rootBundle;

/// This class will have the JSON text data populated in cards.
class DataPageTemplate extends StatelessWidget{
  final String jsonFileName;
  final Color colorScheme;

  const DataPageTemplate({Key key, @required this.jsonFileName, @required this.colorScheme}) : super(key: key);

  //Get Localized JSON Data//
  Future<String> loadLocalData() async {
    if(!jsonFileName.toLowerCase().contains(" ")){
      return await DartService.rootBundle.loadString("assets/json/" + jsonFileName.toLowerCase() + ".json");
    } else {
      return await DartService.rootBundle.loadString("assets/json/" + jsonFileName.replaceAll(" ", "_").toLowerCase() + ".json");
    }
  }

  //Execute JSON Related Services//
  Future<List<KnowledgeCard>> loadLocalJSONData() async{
    //Get Data//
    String sectionData =  await loadLocalData();
    
    //Cast JSON Data//
    final parsedJSON = json.decode(sectionData).cast<Map<String, dynamic>>();

    //parseJSON now has access to the data in list format//
    return parsedJSON.map<KnowledgeCard>((json) => KnowledgeCard.fromJson(json)).toList();
  }

  Card createJSONCards(String title, String data){
    return new Card(
      child: Stack(
        children: <Widget>[
          Align(
             // alignment: Alignment.topLeft,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.maxFinite,
                    color: colorScheme,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15, top: 4, bottom: 4),
                        child: Column(
                          children: <Widget>[
                            RichText(
                              textAlign: TextAlign.left,
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text: title,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                      fontSize: 25,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                  ),
                  Container(
                    width: double.maxFinite,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15, top: 60),
                      child: Column(
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                  text: data,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    color: Colors.grey,
                                    fontSize: 20,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String modifiedTitle = jsonFileName;
    //Title Fix Logic//
    if(!jsonFileName.contains("_")){
      modifiedTitle = jsonFileName.replaceAll("_", " ");
    }

    return Scaffold(
      appBar: AppBar(

        title: Text(modifiedTitle + " Review"),
      ),

      //Create a FutureBuilder return object to get the Local JSON data
      //returned in a list of the model object.
      body: FutureBuilder<List<KnowledgeCard>>(
        future: loadLocalJSONData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              //Create Column return object//
              Column cardColumn = Column(
                children: [],
              );
              //Create SingleChildScrollView Return Object//
              SingleChildScrollView singleChildScrollView =  SingleChildScrollView(
                  child: cardColumn,
              );

              //Create Padding For In Between Cards//
              Padding padding = Padding(
                padding: EdgeInsets.only(bottom: 5),);

              Container adBufferSpace = Container(
                height: 100,
              );

              //Loop through each object to create a display card//
              for (int i=0; i<snapshot.data.length; i++) {
                cardColumn.children.add(createJSONCards(
                    snapshot.data[i].title,
                    snapshot.data[i].data));
                cardColumn.children.add(padding);
              }

              //Add Ad Buffer Space to Bottom//
              cardColumn.children.add(adBufferSpace);

              return singleChildScrollView;
            } else {
              return LinearProgressIndicator();
            }
          }),
    );
  }
}