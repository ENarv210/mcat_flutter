import 'dart:ui';

import 'package:flutter/material.dart';

const Color darkGreen = Color(0xFF0f4539);

const Color blueAccent = Color(0xFF00AAFF);

const Color blueDarkAccent = Color(0xFF0078D7);
const Color purpleAccent = Color(0xFF6869D6);
const Color turquoiseAccent = Color(0xFF00B294);
const Color greenDarkAccent = Color(0xFF647C64);
const Color blueLightAccent = Color(0xFF0099BC);
const Color grayAccent = Color(0xFF69797E);
const Color magentaPink = Color(0xFFCC338B);

const Color darkBlue = Color(0xFF000080);

const int themePrimaryColor = 0xFF000080;
const MaterialColor darkBlueTheme = MaterialColor(
  themePrimaryColor,
  <int, Color>{
    50: Color(0xFFE3F2FD),
    100: Color(0xFFBBDEFB),
    200: Color(0xFF90CAF9),
    300: Color(0xFF64B5F6),
    400: Color(0xFF42A5F5),
    500: Color(0xFF42A5F5),
    600: Color(0xFF1E88E5),
    700: Color(0xFF000090),
    800: Color(0xFF000080),
    900: Color(0xFF000070),
  },
);