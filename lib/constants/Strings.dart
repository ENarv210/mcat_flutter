const String APP_NAME = "MCAT Study";

const String QUESTIONS_OF_THE_DAY = "MCAT Question of the Day";
const String PODCAST = "Podcasts";
const String RESOURCES = "Resources";
const String PSYCHOLOGY = "Psychology";
const String PHYSICS = "Physics";
const String CHEMISTRY = "Chemistry";
const String BIOLOGY = "Biology";
const String BIOCHEM = "BioChem";
const String SOCIOLOGY = "Sociology";
const String CARS = "CARS";
const String GENETICS = "Genetics";
const String ANATOMY = "Anatomy";
const String PHYSIOLOGY = "Physiology";
const String ORGANIC_CHEM = "Organic Chemistry";
const String SELECT_A_SECTION = "Select A Section Below To Begin Reviewing Content";

const String PODCAST_DEFINITION = "Checkout MCAT Related Podcasts";
const String PSYCHOLOGY_DEFINITION = "Behavioral and Sociocultural Systems";
const String BIOLOGY_DEFINITION = "Biology of Living Systems";
const String PHYSICS_DEFINITION = "Physics Concepts and Formulas";
const String CARS_DEFINITION = "Critical Analysis and Reasoning";
const String BIOCHEM_DEFINITION = "Molecular Interactions";
const String CHEMISTRY_DEFINITION = "Basic Chemistry Knowledge";
const String ANATOMY_DEFINITION = "Basic Anatomy";
const String PHYSIOLOGY_DEFINITION = "Basic Physiology";
const String RESOURCES_DEFINITION = "Videos, Mnemonics, and more...";
const String ORGANIC_CHEM_DEFINITION = "Organic Chemistry";
const String GENETICS_DEFINITION = "Genes and Inheritance";